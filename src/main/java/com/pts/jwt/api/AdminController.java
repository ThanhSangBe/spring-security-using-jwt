package com.pts.jwt.api;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
@RestController
@RequestMapping("api/admin")
public class AdminController {
	@GetMapping("users")
	public ResponseEntity<?> getAll()
	{
		return ResponseEntity.ok("api Users");
	}
	@GetMapping("")
	public ResponseEntity<?> index()
	{
		return ResponseEntity.ok("index");
	}
}
