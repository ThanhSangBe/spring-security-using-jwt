package com.pts.jwt.api;

import java.io.IOException;
import java.net.http.HttpResponse;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.mindrot.jbcrypt.BCrypt;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpRequest;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.interfaces.DecodedJWT;
import com.auth0.jwt.interfaces.JWTVerifier;
import com.fasterxml.jackson.core.exc.StreamWriteException;
import com.fasterxml.jackson.databind.DatabindException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.pts.jwt.Entity.Role;
import com.pts.jwt.Entity.User;
import com.pts.jwt.filter.CustomAuthenticationFilter;
import com.pts.jwt.service.UserService;

@RestController
@RequestMapping("/api")
public class UserResource {
	@Autowired
	private UserService userService;
	@GetMapping("/users")
	public ResponseEntity<?> getUsers()
	{
		return ResponseEntity.ok("OKE OKE");
	}
	@GetMapping("apc")
	public ResponseEntity<?> getABC()
	{
		return ResponseEntity.ok("ABC");
	}
	@GetMapping("/checkPass")
	public ResponseEntity<?> checkPass(@RequestBody String body)
	{
		String hash = BCrypt.hashpw(body, BCrypt.gensalt(12));
		boolean valid = BCrypt.checkpw(body, hash);
		return ResponseEntity.ok(valid);
	}
	@PreAuthorize("hasRole('USER')")
	@PostMapping("/user")
	public ResponseEntity<?> saveUser(@RequestBody User user)
	{
		return ResponseEntity.ok(userService.saveUser(user));
	}
	@PreAuthorize("hasAnyRole('ADMIN', 'USER')")
	@GetMapping("/user/{id}")
	public ResponseEntity<?> getUser(@PathVariable long id)
	{
		return ResponseEntity.ok(this.userService.findById(id));
	}
	
	@PreAuthorize("hasAuthority('USER')")
	@GetMapping("/user/test")
	public ResponseEntity<?> getUsersa()
	{
		return ResponseEntity.ok("USER");
	}

	
	@PostMapping("/role")
	public ResponseEntity<?> saveRole(@RequestBody Role role)
	{
		return ResponseEntity.ok(userService.saveRole(role));
	}
	@GetMapping("refreshToken/{body}")
	public void refeshToken(HttpServletRequest request, HttpServletResponse response, @PathVariable String body) throws StreamWriteException, DatabindException, IOException
	{
		String authorizationHeader = request.getHeader("AUTHORIZATION");//LAY AUTHORIZATION TRONG HEADER
		if(authorizationHeader != null && authorizationHeader.startsWith("Bearer"))// DUNG FORMAT
		{
			try {
				String refresh_token = authorizationHeader.substring("Bearer ".length());
				//THUAT TOAN BAM
				Algorithm algorithm = Algorithm.HMAC256(CustomAuthenticationFilter.keyAlgorithm.getBytes());
				JWTVerifier verifier = JWT.require(algorithm).build();//Xac minh
				DecodedJWT decodedJWT = verifier.verify(refresh_token);
				String username = decodedJWT.getSubject();//GET USERNAME 
				User user = userService.getUser(username);
				String access_token = JWT.create()
						.withSubject(user.getUsername())
						.withExpiresAt(new Date(System.currentTimeMillis() + 30*60*1000))
						.withIssuer(request.getRequestURI().toString())
						.withClaim("roles", user.getRoles().stream().map(Role:: getName).collect(Collectors.toList()))
						.sign(algorithm);
				
				response.setHeader("access_token", access_token);
				response.setHeader("refesh_token", refresh_token);
				Map<String, String> maps = new HashMap<String, String>();
				maps.put("access_token", access_token);
				maps.put("refesh_token", refresh_token);
				maps.put("username", user.getUsername());
				maps.put("role", user.getRoles().toString());
				response.setContentType("APPLICATION_JSON_VALUE");//SET BODY JSON, NEU KHONG SET MAC DINH SE LA TEXT
				new ObjectMapper().writeValue(response.getOutputStream(), maps);//GHI RA BODY
			}catch (Exception e) {
				response.setHeader("error", e.getMessage());
				Map<String, String> maps = new HashMap<String, String>();
				String error = "YOU NOT ALLOW";
				maps.put("error", e.getMessage());
				maps.put("message", error);
				response.setContentType("APPLICATION_JSON_VALUE");//SET BODY JSON, NEU KHONG SET MAC DINH SE LA TEXT
				new ObjectMapper().writeValue(response.getOutputStream(), maps);//GHI RA BODY
			}
		}
		else 
		{
			throw new RuntimeException("Refesh token is missing");
		}
		
	}
	@PostConstruct
	public void prepare()
	{
		userService.saveRole(new Role(0, "ROLE_USER"));
		userService.saveRole(new Role(0, "ROLE_ADMIN"));
		userService.saveRole(new Role(0, "ROLE_MANAGER"));
		userService.saveRole(new Role(0, "ROLE_SUPER_ADMIN"));
		
		userService.saveUser(new User(0, "Kelvin Sang", "Kelvin", "123", new ArrayList<>()));
		userService.saveUser(new User(0, "John Athon", "Athon", "123", new ArrayList<>()));
		userService.saveUser(new User(0, "ZoKoBus", "zokobus", "123", new ArrayList<>()));
		userService.saveUser(new User(0, "Toliver", "Toliver", "123", new ArrayList<>()));
		userService.saveUser(new User(0, "Subin Hoang Son", "Subin", "123", new ArrayList<>()));
		userService.saveUser(new User(0, "Jim Carry", "Carry", "123", new ArrayList<>()));
		
		userService.addRoleToUser("Kelvin", "ROLE_USER");
		userService.addRoleToUser("Kelvin", "ROLE_ADMIN");
		userService.addRoleToUser("Athon", "ROLE_USER");
		userService.addRoleToUser("zokobus", "ROLE_MANAGER");
		userService.addRoleToUser("Carry", "ROLE_ADMIN");
		userService.addRoleToUser("Subin", "ROLE_SUPER_ADMIN");

	}
//eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJLZWx2aW4iLCJyb2xlcyI6WyJST0xFX0FETUlOIiwiUk9MRV9VU0VSIl0sImlzcyI6Ii9hcGkvbG9naW4ifQ._PTo1KcqoSohIhffAS4D6lmxx4auzEGG_DuhK8iXXzM	
	}
