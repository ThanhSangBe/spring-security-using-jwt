package com.pts.jwt.api;

import java.util.Map;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/test")
public class HomeController {
	@GetMapping("")
	public ResponseEntity<?> index()
	{
		return ResponseEntity.ok("Method Index");
	}
	@GetMapping("/{id}/{name}")
	public ResponseEntity<?> get(@PathVariable Map<String, String> param)
	{
		return ResponseEntity.ok(param.getOrDefault("id", "ID") + param.getOrDefault("name", "NAME"));
	}
}
