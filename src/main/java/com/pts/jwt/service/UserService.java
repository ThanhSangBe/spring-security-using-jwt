package com.pts.jwt.service;

import java.util.List;
import java.util.Optional;

import com.pts.jwt.Entity.Role;
import com.pts.jwt.Entity.User;

public interface UserService {
	User saveUser(User user);
	Role saveRole(Role role);
	void addRoleToUser(String username, String role);
	User getUser(String username);
	List<User> getUsers();
	long count();
	Optional<User> findById(Long id);
	List<User> findAll();
	<S extends User> S save(S entity);
	User findByUsername(String username);
}
