package com.pts.jwt.service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import com.pts.jwt.Entity.Role;
import com.pts.jwt.Entity.User;
import com.pts.jwt.repo.RoleRepo;
import com.pts.jwt.repo.UserRepo;

import lombok.extern.slf4j.Slf4j;
@Service
@Slf4j
public class UserServiceImpl implements UserService,UserDetailsService {
	@Autowired
	private UserRepo userRepo;
	@Autowired
	private RoleRepo roleRepo;
	@Autowired
	private PasswordEncoder passwordEncoder;
	@Override
	public User saveUser(User user) {
		String password = user.getPassword();
		String encoderPassword = passwordEncoder.encode(password);
		user.setPassword(encoderPassword);
		User entity = this.userRepo.save(user);
		log.info("Save new user {} to database",user.getUsername());
		return entity;
	}

	@Override
	public User findByUsername(String username) {
		return userRepo.findByUsername(username);
	}

	@Override
	public <S extends User> S save(S entity) {
		return userRepo.save(entity);
	}

	@Override
	public List<User> findAll() {
		return userRepo.findAll();
	}

	@Override
	public Optional<User> findById(Long id) {
		return userRepo.findById(id);
	}

	@Override
	public long count() {
		return userRepo.count();
	}

	@Override
	public Role saveRole(Role role) {
		Role entity = this.roleRepo.save(role);
		log.info("Save new role {} to the database",role.getName());
		return entity;
	}

	@Override
	public void addRoleToUser(String username, String rolename) {
		log.info("Adding role {} to user {}",rolename,username);
		User user = userRepo.findByUsername(username);
		Role role = roleRepo.findByName(rolename);
		user.getRoles().add(role);
		userRepo.save(user);
		
	}

	@Override
	public User getUser(String username) {
		return userRepo.findByUsername(username);
	}

	@Override
	public List<User> getUsers() {
		return userRepo.findAll();
	}

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		User user = userRepo.findByUsername(username);
		if(user == null)
		{	log.error("User incorrect!");
			throw new UsernameNotFoundException(String.format("User not found with username: %s",username));
		}
			
		Collection<SimpleGrantedAuthority> authorities = new ArrayList<SimpleGrantedAuthority>();
		user.getRoles().forEach(
				role -> authorities.add(new SimpleGrantedAuthority(role.getName()))
				);
		org.springframework.security.core.userdetails.User entity = new org.springframework.security.core.userdetails.User(user.getUsername(), user.getPassword(), authorities);
		return entity;
	}
	

}
