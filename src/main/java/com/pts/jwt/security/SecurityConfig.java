package com.pts.jwt.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.access.AccessDeniedHandler;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import com.pts.jwt.filter.CustomAccessDeniedHandler;
import com.pts.jwt.filter.CustomAuthenticationFilter;
import com.pts.jwt.filter.CustomAuthorizationFilter;

import lombok.RequiredArgsConstructor;

@Configuration
@EnableWebSecurity
@RequiredArgsConstructor
public class SecurityConfig extends WebSecurityConfigurerAdapter {
	@Autowired
	private UserDetailsService userDetailsService;


	@Bean
	public PasswordEncoder passwordEncoder()// Dùng mã hóa password để lưu mật khẩu 
	{
		return new BCryptPasswordEncoder();
	}

	@Override
	protected void configure(AuthenticationManagerBuilder auth) throws Exception //ÁP DỤNG ĐỂ SO SÁNH MẬT KHẨU
	{
		auth.userDetailsService(userDetailsService).passwordEncoder(passwordEncoder());
	}

	@Override
	//Hàm này cực kỳ quan trọng để cấu hình những thông tin. 
	protected void configure(HttpSecurity http) throws Exception 
	{
		CustomAuthenticationFilter customAuthenticationFilter = new CustomAuthenticationFilter(authenticationManagerBean());
		customAuthenticationFilter.setFilterProcessesUrl("/api/login");
		http.csrf().disable();
		http.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS);
		http.authorizeRequests().antMatchers("/api/login/**","/api/refreshToken/**","api/checkPass/**").permitAll();
//		http.authorizeRequests().antMatchers(HttpMethod.POST,"/api/user/**").hasAnyAuthority("ROLE_USER");
//		http.authorizeRequests().antMatchers("/api/admin/**").
		http.authorizeRequests().antMatchers("/api/user/**").access("hasRole('ROLE_USER')");
		http.authorizeRequests().antMatchers("/api/admin/**").access("hasRole('ROLE_ADMIN')");
		http.authorizeRequests().antMatchers("/api/test/**").permitAll();
		http.authorizeRequests().anyRequest().authenticated();//KHONG CAN DANG NHAP ||PHAI DANG NHAP
		http.addFilter(customAuthenticationFilter);//CUSTOM FILTER AUTHENTICATION, CUSTOM XAC THUC
		http.exceptionHandling().accessDeniedHandler(accessDeniedHandler());
		http.addFilterBefore(new CustomAuthorizationFilter(), UsernamePasswordAuthenticationFilter.class);
	}
	@Bean
	@Override
	public AuthenticationManager authenticationManagerBean() throws Exception {
		// TODO Auto-generated method stub
		return super.authenticationManagerBean();
	}
	@Bean
	public AccessDeniedHandler accessDeniedHandler(){
	    return new CustomAccessDeniedHandler();
	}
}
