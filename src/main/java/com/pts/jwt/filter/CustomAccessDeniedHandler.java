package com.pts.jwt.filter;

import static org.springframework.http.HttpStatus.FORBIDDEN;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.web.access.AccessDeniedHandler;

import com.fasterxml.jackson.databind.ObjectMapper;

public class CustomAccessDeniedHandler implements AccessDeniedHandler {

	@Override
	public void handle(HttpServletRequest request, HttpServletResponse response,
			AccessDeniedException accessDeniedException) throws IOException, ServletException {

		response.setStatus(FORBIDDEN.value());
		Map<String, String> maps = new HashMap<String, String>();
		String error = "Không có quyền truy cập";
		maps.put("message", error);
		maps.put("httpCode", String.valueOf(FORBIDDEN.value()));
		response.setContentType("APPLICATION_JSON_VALUE");//SET BODY JSON, NEU KHONG SET MAC DINH SE LA TEXT
		new ObjectMapper().writeValue(response.getOutputStream(), maps);//GHI RA BODY
		
	}

}
