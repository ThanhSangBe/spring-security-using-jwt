package com.pts.jwt.filter;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.client.HttpClientErrorException.Forbidden;
import org.springframework.web.filter.OncePerRequestFilter;

import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.interfaces.DecodedJWT;
import com.auth0.jwt.interfaces.JWTVerifier;
import com.fasterxml.jackson.databind.ObjectMapper;
import static org.springframework.http.HttpStatus.FORBIDDEN;

import lombok.extern.slf4j.Slf4j;
//CUSTOM PHÂN QUYỀN TRUY CẬP
@Slf4j
public class CustomAuthorizationFilter extends OncePerRequestFilter{

	@Override
	protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain)
			throws ServletException, IOException {

			String uri = request.getServletPath();
			if(	uri.equals("/api/login") 
				|| uri.startsWith("/api/refreshToken")  
				|| uri.equals("/api/checkPass") 
				|| uri.startsWith("/api/test") 
				|| uri.equals("/api/users"))//CHO PHÉP TRUY CẬP MÀ KHÔNG CẦN PHÂN QUYỀN
			{
				filterChain.doFilter(request, response);
			}
			else
			{
				String authorizationHeader = request.getHeader("AUTHORIZATION");//Get AUTHORIZATION in HEADER
				// Kiểm tra có token hay không và check ký tự Bearer để xác thực phân quyền
				if(authorizationHeader != null && authorizationHeader.startsWith("Bearer"))
				{
					try {
						//lay token
						String token = authorizationHeader.substring("Bearer ".length());
						//THUAT TOAN BAM
						Algorithm algorithm = Algorithm.HMAC256(CustomAuthenticationFilter.keyAlgorithm.getBytes());
						JWTVerifier verifier = JWT.require(algorithm).build();//thiết lập cổng bảo vệ xác nhận
						DecodedJWT decodedJWT = verifier.verify(token);// Thực hiện xác minh, nếu không đúng sẽ throw ra lỗi.giống như người canh gác 
						String username = decodedJWT.getSubject();//GET USERNAME 
						String[] roles = decodedJWT.getClaim("roles").asArray(String.class);//GET ROLES
						Collection<SimpleGrantedAuthority> authorities = new ArrayList<SimpleGrantedAuthority>();
						for (String item : roles) {
							authorities.add(new SimpleGrantedAuthority(item));
						}
						UsernamePasswordAuthenticationToken authenticationToken = 
									new UsernamePasswordAuthenticationToken(username,null,authorities);
						SecurityContextHolder.getContext().setAuthentication(authenticationToken);
						filterChain.doFilter(request, response);
						}catch (Exception e) {
						log.error("Error loggin in: {}",e.getMessage());
						response.setHeader("error", e.getMessage());
						response.setStatus(FORBIDDEN.value());
						Map<String, String> maps = new HashMap<String, String>();
						String error = "Đăng nhập thất bại";
						maps.put("message", error);
						maps.put("message system", e.getMessage());
						maps.put("httpCode", String.valueOf(FORBIDDEN.value()));
						response.setContentType("APPLICATION_JSON_VALUE");//SET BODY JSON, NEU KHONG SET MAC DINH SE LA TEXT
						new ObjectMapper().writeValue(response.getOutputStream(), maps);//GHI RA BODY
					}
				}
				else 
				{
					response.setHeader("error", "ERROR");
					response.setStatus(FORBIDDEN.value());
					Map<String, String> maps = new HashMap<String, String>();
					String error = "YOU NOT ALLOW";
					maps.put("message", error);
					maps.put("httpCode", String.valueOf(FORBIDDEN.value()));
					response.setContentType("APPLICATION_JSON_VALUE");//SET BODY JSON, NEU KHONG SET MAC DINH SE LA TEXT
					new ObjectMapper().writeValue(response.getOutputStream(), maps);//GHI RA BODY
				}
			}
		
		
		
	}

}
