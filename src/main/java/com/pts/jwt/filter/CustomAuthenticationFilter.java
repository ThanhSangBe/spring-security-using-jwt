package com.pts.jwt.filter;

import java.io.IOException;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import com.fasterxml.jackson.databind.ObjectMapper;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class CustomAuthenticationFilter extends UsernamePasswordAuthenticationFilter {
	public static String keyAlgorithm = "secret";
	@Autowired
	private AuthenticationManager authenticationManager;

	public CustomAuthenticationFilter(AuthenticationManager authenticationManager) {
		this.authenticationManager = authenticationManager;
	}
	//Xác thực tài khoản tại đây
	@Override
	public Authentication attemptAuthentication(HttpServletRequest request, HttpServletResponse response)
			throws AuthenticationException {
		String username = request.getParameter("username");
		String password = request.getParameter("password");
		log.info("Username is :{}", username);
		log.info("Password is:{}", password);
		UsernamePasswordAuthenticationToken authenticationToken = new UsernamePasswordAuthenticationToken(username,password);
		return authenticationManager.authenticate(authenticationToken);
	}
	// Xác thực thành công
	@Override
	protected void successfulAuthentication(HttpServletRequest request, HttpServletResponse response, FilterChain chain, Authentication authResult) 
		throws IOException, ServletException {
		User user = (User) authResult.getPrincipal();
		Algorithm algorithm = Algorithm.HMAC256(keyAlgorithm.getBytes());//THUAT TOAN HMAC256 SECRET
//		long timeAccessToken = System.currentTimeMillis() + 30*60*1000;//HSD 30p
//		long timeRefreshToken = System.currentTimeMillis() + 10*24*60*60*1000;//HSD 10 ngay
		String access_token = JWT.create()
				.withSubject(user.getUsername())
//				.withExpiresAt(new Date(timeAccessToken))// thiết lập giá trị hạn sử dụng
				.withIssuer(request.getRequestURI().toString())
				.withClaim("roles", user.getAuthorities().stream().map(GrantedAuthority:: getAuthority).collect(Collectors.toList()))
				.sign(algorithm);
		String refesh_token = JWT.create()
				.withSubject(user.getUsername())
//				.withExpiresAt(new Date(timeRefreshToken))// thiết lập giá trị hạn sử dụng	
				.withIssuer(request.getRequestURI().toString())
				.withClaim("roles", user.getAuthorities().stream().map(GrantedAuthority:: getAuthority).collect(Collectors.toList()))
				.sign(algorithm);//Chữ ký
		//Trường hợp nếu không lưu giá trị hết hạn thì chỉ cần dùng 1 token thôi là được rồi
		response.setHeader("access_token", access_token);
		response.setHeader("refesh_token", refesh_token);
		Map<String, String> maps = new HashMap<String, String>();
		maps.put("access_token", access_token);
		maps.put("refesh_token", refesh_token);
		maps.put("username", user.getUsername());
		log.info("User login is successfull with {}",user.getUsername());
		maps.put("role", user.getAuthorities().toString());
		response.setContentType("APPLICATION_JSON_VALUE");//SET BODY JSON, NEU KHONG SET MAC DINH SE LA TEXT
		new ObjectMapper().writeValue(response.getOutputStream(), maps);//GHI RA BODY
	}
	//Xác thực thất bại, trả ra lỗi
	@Override
	protected void unsuccessfulAuthentication(HttpServletRequest request, HttpServletResponse response,
			AuthenticationException failed) throws IOException, ServletException {
		Map<String, String> maps = new HashMap<String, String>();
		maps.put("message", "Tài khoản không chính xác");
		response.setContentType("APPLICATION_JSON_VALUE");//SET BODY JSON, NEU KHONG SET MAC DINH SE LA TEXT
		new ObjectMapper().writeValue(response.getOutputStream(), maps);//GHI RA BODY
	}
	
}
